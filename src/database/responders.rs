use rocket::{Request, Response};
use rocket::http::Status;
use std::io::Cursor;
use serde::{Serialize};

pub struct CommonResponders {}
impl CommonResponders {
    pub fn json_responder<T>(type_instance: T, _: &Request) -> Result<Response<'static>, Status> where T: Serialize {
        let body_content = serde_json::to_string(&type_instance).unwrap();
        Response::build()
            .raw_header("Content-Type", "application/json")
            .sized_body(Cursor::new(body_content))
            .ok()
    }
}