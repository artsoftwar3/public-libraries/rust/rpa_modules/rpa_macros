/**
    Rpa (Rust Persistence API) Trait definition
    Copyright (C) 2019  Jonathan Franco

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

    Association Methods
    These kind of methods can't be defined in a trait because those are dynamically generated
    but here we have some documentation on how those are generated


    To find from parent we have this:

    let children: Vec<Child> = Child::find_for_parent_name(&parent_instance, &*_connection).unwrap();

    If Parent is called Hero and we have a child called Post we can use this:

    let posts: Vec<Post> = Post::find_for_hero(&hero, &*_connection).unwrap();

    To find grouped by we have this:

    let children: Vec<(Parent, Vec<Child>)> = Child::find_grouped_by_parent_name(&parent_instance, &*_connection).unwrap();

    If Parent is called Hero and we have a child called Post we can use this:

    let posts: Vec<(Hero, Vec<Post>)> = Post::find_grouped_by_hero(&heroes, &*_connection).unwrap();
**/
use rocket_contrib::json::Json;
use crate::{RpaError, SearchResponse};
use crate::database::search::search_request::SearchRequest;

pub trait Rpa<T, C> where C: diesel::Connection {
    fn into_json(self) -> Json<T>;
    fn from_json(json: Json<T>) -> T;

    fn save(entity: &T, connection: &C) -> Result<T, RpaError>;
    fn save_self(self: Self, connection: &C) -> Result<T, RpaError>;
    fn save_batch(entities: Vec<T>, connection: &C) -> Result<usize, RpaError>;
    fn find(entity_id: &String, connection: &C) -> Result<T, RpaError>;
    fn find_all(connection: &C) -> Result<Vec<T>, RpaError>;
    fn exists(entity_id: &String, connection: &C) -> Result<bool, RpaError>;
    fn update(entity_id: &String, entity: &T, connection: &C) -> Result<usize, RpaError>;
    fn update_self(self: Self, connection: &C) -> Result<usize, RpaError>;
    fn delete(entity_id: &String, connection: &C) -> Result<usize, RpaError>;
    fn delete_self(self: Self, connection: &C) -> Result<usize, RpaError>;

    fn search(search_request: SearchRequest, connection: &C) -> Result<SearchResponse<T>, RpaError>;
}
