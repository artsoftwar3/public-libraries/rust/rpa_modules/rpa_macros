pub trait SqlSymbol {
    fn symbol(self) -> & 'static str;
}