use serde::{Serialize, Deserialize};
use crate::database::sql::enums::sql_symbol::SqlSymbol;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum WhereJoiner {
    OR, AND
}

impl SqlSymbol for WhereJoiner {
    fn symbol(self) -> &'static str {
        return match self {
            WhereJoiner::OR => "OR",
            WhereJoiner::AND => "AND"
        }
    }
}