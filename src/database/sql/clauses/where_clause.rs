use serde::{Serialize, Deserialize};
use crate::database::sql::enums::operator::Operator;
use crate::database::sql::enums::where_joiner::WhereJoiner;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct WhereClause {
    pub field_name: String,
    pub operator: Operator,
    pub value: String,
    pub joiner: WhereJoiner
}