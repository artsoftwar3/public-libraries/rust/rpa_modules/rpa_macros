#[derive(Clone, Debug)]
pub struct Query {
    pub sql: String,
    pub page: Option<i64>,
    pub page_size: Option<i64>,
    pub total_pages: Option<i64>,
}