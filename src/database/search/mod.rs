use crate::database::search::search_request::SearchRequest;
use diesel::{MysqlConnection, sql_query, RunQueryDsl};
use crate::database::search::search_response::SearchResponse;
use crate::RpaError;
use crate::database::search::validators::validate_request;
use crate::database::search::utils::to_query_builder;

pub mod fields;
pub mod search_request;
pub mod search_response;
pub mod pagination;
pub mod validators;
pub mod utils;

pub fn do_search<T>(request: SearchRequest, table_name: String, connection: &MysqlConnection) -> Result<SearchResponse<T>, RpaError> where T: diesel::deserialize::QueryableByName<diesel::mysql::Mysql> {
    validate_request(request.clone())?;
    let query_builder = to_query_builder(request.clone(), table_name);
    let query = query_builder.build(connection).unwrap();
    debug!("Querying SQL {}", query.sql.as_str());
    let query_result = sql_query(query.sql.as_str()).load(connection);
    if query_result.is_err() {
        let error = query_result.err().unwrap();
        let error_message = error.to_string().clone();
        return Err(RpaError::builder()
                        .with_cause(error)
                        .with_description(error_message.as_str())
                        .build());
    }
    let query_result = query_result.unwrap();
    Ok(SearchResponse {
        results: query_result,
        total_pages: query.total_pages,
        page: query.page,
        page_size: query.page_size
    })
}



