use serde::{Serialize, Deserialize};
use crate::database::sql::enums::operator::Operator;
use crate::database::sql::enums::where_joiner::WhereJoiner;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct FilterField {
    pub name: String,
    pub value: String,
    pub operator: Operator,
    pub joiner: WhereJoiner
}