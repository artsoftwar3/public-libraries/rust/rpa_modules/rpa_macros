use serde::{Serialize, Deserialize};
use crate::database::sql::enums::order::Order;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct SortField {
    pub name: String,
    pub order: Order,
}