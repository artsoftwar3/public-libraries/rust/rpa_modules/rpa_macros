use crate::database::search::search_request::SearchRequest;
use crate::database::sql::query_builder::QueryBuilder;

pub fn to_query_builder(request: SearchRequest, table_name: String) -> QueryBuilder {
    let mut query_builder = QueryBuilder::new(table_name);
    for filter_field in request.filter_fields {
        query_builder = query_builder.where_values(filter_field.name, filter_field.operator, filter_field.value, filter_field.joiner);
    }
    for sort_field in request.sort_fields {
        query_builder = query_builder.order_by(sort_field.name, sort_field.order);
    }
    if request.pagination.is_some() {
        query_builder = query_builder.with_pagination(request.pagination);
    }
    query_builder
}