use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct SearchResponse<T> {
    pub results: Vec<T>,
    #[serde(skip_serializing_if = "Option::is_none", rename = "totalPages")]
    pub total_pages: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none", rename = "pageSize")]
    pub page_size: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub page: Option<i64>
}