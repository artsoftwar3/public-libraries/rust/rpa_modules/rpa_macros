/**
    Rpa (Rust Persistence API) Macro Module
    Copyright (C) 2019  Jonathan Franco

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/

#[macro_use] extern crate diesel;
#[macro_use] extern crate log;

pub mod database;
pub use database::error::RpaError;
pub use database::{
    search::{
        search_request::SearchRequest,
        search_response::SearchResponse,
        fields::{
            filter_field::FilterField,
            sort_field::SortField
        },
        pagination::Pagination
    },
    sql::{
        clauses::{
            having_clause::HavingClause,
            join_clause::JoinClause,
            order_by_clause::OrderByClause,
            where_clause::WhereClause
        },
        enums::{
            operator::Operator,
            join_type::JoinType,
            order::Order,
            where_joiner::WhereJoiner
        }
    }
};
pub use database::search::do_search;
