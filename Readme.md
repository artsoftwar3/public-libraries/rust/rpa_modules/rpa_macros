<div align="center">
    <img src="logo.png" height="200">
</div>

<a href="https://crates.io/crates/rpa_macros">![RPA Macros Version](https://img.shields.io/badge/crates.io-v0.5.1-orange.svg?longCache=true)</a>


### RPA Macros
This library is a module of the Rust Persistence API.
Please visit [rpa](https://gitlab.com/artsoftwar3/public-libraries/rust/rpa) for more information.
